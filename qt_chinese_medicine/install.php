<?php

/*
	Xiuno BBS 4.0 插件：中医常用数据库插件安装
	admin/plugin-install-qt_chinese_medicine.htm
*/

!defined('DEBUG') AND exit('Forbidden');
$cm = array('zhongyao', 'fangji', 'jirou', 'shuxue','shhzbl');
foreach($cm as $name) {
install_sql_file(APP_PATH . 'plugin/qt_chinese_medicine/install_'.$name.'.sql');
}
message(1, 'ok');
function install_sql_file($sqlfile) {
	global $errno, $errstr;
	$s = file_get_contents($sqlfile);
	//$s = str_replace(";\r\n", ";\n", $s);
	$s = preg_replace('/#(.*?)\r\n/i', "", $s);
	$arr = explode(";\r\n", $s);
	foreach ($arr as $sql) {
		$sql = trim($sql);
		if(empty($sql)) continue;
		$arr = explode(";\n", $s);
		db_exec($sql) === FALSE AND message(-1, "sql: $sql, errno: $errno, errstr: $errstr");
	}
}


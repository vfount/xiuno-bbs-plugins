	if($ajax) {
		$thread = thread_safe_info($thread);
		if($page == 1) {
			$postlist = array_merge(array($first), $postlist);
		}
		foreach($postlist as &$post) {
			$post = post_safe_info($post);
		}
		message(0, 'ok', array(
			'page'=>$page,
			'thread'=>$thread,
			'postlist'=>array_values($postlist),
			'allowpost'=>$allowpost,
			'allowupdate'=>$allowupdate,
			'allowdelete'=>$allowdelete,
		));
	}

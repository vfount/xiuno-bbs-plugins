<?php

/*
	Xiuno BBS 4.0 插件实例：友情链接插件安装
	admin/plugin-install-qt_signature.htm
*/

!defined('DEBUG') AND exit('Forbidden');

$tablepre = $db->tablepre;
$column_exist = db_sql_find_one("show columns from {$tablepre}user like 'signature';");
if($column_exist === false) {
$r = db_exec("ALTER TABLE {$tablepre}user ADD COLUMN signature varchar(256) NOT NULL DEFAULT ''");
$r === FALSE AND message(-1, '添加签名字段失败');
}

?>
;(function() {

var accessKeyXSelector = 'a.page-link, a.page-link, .subject a[href*=thread], .message';
var accessKeyZSelector = 'textarea, a[href*=thread-create], .nav-link, a[href*=user-log], a[href*=my]';
 
// copy from current-device
var device = {}

// The client user agent string.
// Lowercase, so we can use the more efficient indexOf(), instead of Regex
var userAgent = window.navigator.userAgent.toLowerCase()

// Detectable television devices.
var television = [
  'googletv',
  'viera',
  'smarttv',
  'internet.tv',
  'netcast',
  'nettv',
  'appletv',
  'boxee',
  'kylo',
  'roku',
  'dlnadoc',
  'pov_tv',
  'hbbtv',
  'ce-html'
]

// Main functions
// --------------

device.macos = function() {
  return find('mac')
}

device.ios = function() {
  return device.iphone() || device.ipod() || device.ipad()
}

device.iphone = function() {
  return !device.windows() && find('iphone')
}

device.ipod = function() {
  return find('ipod')
}

device.ipad = function() {
  return find('ipad')
}

device.android = function() {
  return !device.windows() && find('android')
}

device.androidPhone = function() {
  return device.android() && find('mobile')
}

device.androidTablet = function() {
  return device.android() && !find('mobile')
}

device.blackberry = function() {
  return find('blackberry') || find('bb10') || find('rim')
}

device.blackberryPhone = function() {
  return device.blackberry() && !find('tablet')
}

device.blackberryTablet = function() {
  return device.blackberry() && find('tablet')
}

device.windows = function() {
  return find('windows')
}

device.windowsPhone = function() {
  return device.windows() && find('phone')
}

device.windowsTablet = function() {
  return device.windows() && (find('touch') && !device.windowsPhone())
}

device.fxos = function() {
  return (find('(mobile') || find('(tablet')) && find(' rv:')
}

device.fxosPhone = function() {
  return device.fxos() && find('mobile')
}

device.fxosTablet = function() {
  return device.fxos() && find('tablet')
}

device.meego = function() {
  return find('meego')
}

device.cordova = function() {
  return window.cordova && location.protocol === 'file:'
}

device.nodeWebkit = function() {
  return typeof window.process === 'object'
}

device.mobile = function() {
  return (
    device.androidPhone() ||
    device.iphone() ||
    device.ipod() ||
    device.windowsPhone() ||
    device.blackberryPhone() ||
    device.fxosPhone() ||
    device.meego()
  )
}

device.tablet = function() {
  return (
    device.ipad() ||
    device.androidTablet() ||
    device.blackberryTablet() ||
    device.windowsTablet() ||
    device.fxosTablet()
  )
}

device.desktop = function() {
  return !device.tablet() && !device.mobile()
}

device.television = function() {
  let i = 0
  while (i < television.length) {
    if (find(television[i])) {
      return true
    }
    i++
  }
  return false
}


// Private Utility Functions
// -------------------------

// Simple UA string search
function find(needle) {
  return userAgent.indexOf(needle) !== -1
}


// Public functions to get the current value of type, os, or orientation
// ---------------------------------------------------------------------

function findMatch(arr) {
  for (var i = 0; i < arr.length; i++) {
    if (device[arr[i]]()) {
      return arr[i]
    }
  }
  return 'unknown'
}

device.type = findMatch(['mobile', 'tablet', 'desktop'])
device.os = findMatch([
  'ios',
  'iphone',
  'ipad',
  'ipod',
  'android',
  'blackberry',
  'windows',
  'fxos',
  'meego',
  'television'
])
//copy end
function isVisible(t) {
//return !!((t.tabIndex >= 0 || t.hasAttribute && t.hasAttribute('tabindex') && t.getAttribute('tabindex') == '-1') &&
return !!(!t.hasAttribute('disabled') &&
t.getAttribute('aria-hidden') !== 'true' &&
t.offsetParent !== null);
}
function isVisibleOld(el) {
    var rect = el.getBoundingClientRect();
    return !!(
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
}

function addAccesskeyAttribute(selector, accesskey) {
var els = document.querySelectorAll(selector);
for(var i = 0, len = els.length; i < len; i++) {
if(els[i] !== null) {
els[i].setAttribute('accessKey', accesskey);
}
}
}
function addClass(selector, clsName) {
var els = document.querySelectorAll(selector);
for(var i = 0, len = els.length; i < len; i++) {
if(els[i] !== null) {
els[i].classList.add(clsName);
}
}
}
var els = document.all; //Array.prototype.slice.call(document.all);
function toFocus(selector, op) {
var len = els.length;
var ae = document.activeElement;
var aeIndex = index = 0; //els.indexOf(ae);
for(var i = 0; i < len; i++) {
if(els[i] == ae) {
aeIndex = index = i;
break;
}
}
var i = op == '+' ? index + 1 : index - 1;
while(i != aeIndex) {
if(els[i].classList.contains(selector) && isVisible(els[i])) {
index = i;
break;
}
i = op == '+' ? i + 1 : i - 1;
if(i >= len) {
i = 0;
}
if(i < 0) {
i = len -1;
}
}
if(index == aeIndex) {
return;
}
var tagName = els[index].tagName.toLowerCase();
if(tagName == 'div' || tagName == 'span' || tagName == 'p') {
if(!els[index].hasAttribute('tabindex')) {
els[index].setAttribute('tabindex', '-1');
}
}
els[index].focus();
}
function nextFocus(selector) {
toFocus(selector, '+');
}

function previousFocus(selector) {
toFocus(selector, '-');
}

function isIE() {
return (!!window.ActiveXObject || "ActiveXObject" in window);
}

if(device.desktop && isIE()) {
//alert('b');
addAccesskeyAttribute(accessKeyXSelector, 'x');
addAccesskeyAttribute(accessKeyZSelector, 'z');
//alert('c');
} else if(device.desktop()) {
//alert('d');
addClass(accessKeyXSelector, 'accesskey-x');
addClass(accessKeyZSelector, 'accesskey-z');
//alert('e');
document.addEventListener('keydown', function(e) {
if(e.altKey && e.shiftKey && e.keyCode == 88) {
previousFocus('accesskey-x');
return false;
}
if(e.altKey && e.keyCode == 88) {
nextFocus('accesskey-x');
return false;
}

if(e.altKey && e.shiftKey && e.keyCode == 90) {
previousFocus('accesskey-z');
return false;
}
if(e.altKey && e.keyCode == 90) {
nextFocus('accesskey-z');
return false;
}
}, null);
}
})();

$('i[class*=icon]').attr('aria-hidden', 'true');
// hide the avatar
$('.thread a[tabindex=-1], .card-thread a[tabindex=-1], .post a[tabindex=-1]').attr('aria-hidden', 'true');
$('.thread span:contains("←")').attr('aria-hidden', 'true');
$('.floor-parent').attr('aria-hidden', 'true');
$('.message').each(function(i){
var _this = $(this);
var ele = _this.parents('.media-body');
var username = ele.find('.username');
username = username ? username.text() : '';
var floor = ele.find('.floor');
 floor = floor ? floor.text() : '';
var text = '';
if(username.trim() == '') {
var tu = $('.card-thread .username a');
text = tu ? tu.text() + ' 1 楼' : '楼主';
//text = '楼主';
} else {
text = username + ' ' + floor + ' 楼';
}
_this.prepend('<span class="sr-only">' + text + '</span>');
_this.attr({
"tabindex": "-1"
});
});
$('.message:last').removeAttr('tabindex').removeAttr('accesskey').removeClass('accesskey-x').find('span.sr-only').remove();
$('.post_reply span').removeClass('d-none').addClass('sr-only');
$(window).on('load', function(e) {
$('.message:first, input[name=email]').focus();
setTimeout(function() {
$('input[name=subject]').focus();
}, 200);
});

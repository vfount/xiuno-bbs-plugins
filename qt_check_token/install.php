<?php

/*
	Xiuno BBS 4.0 插件：单点登录安装
	admin/plugin-install-qt_check_token.htm
*/

!defined('DEBUG') AND exit('Forbidden');

$tablepre = $db->tablepre;
$sql = "ALTER TABLE {$tablepre}user ADD COLUMN token varchar(200) NOT NULL default '';";
db_exec($sql);

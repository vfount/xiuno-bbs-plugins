<?php
$plugins = glob('./*/conf.json');
$plcs = array();
foreach($plugins as $plugin) {
$conf = file_get_contents($plugin);
$conf = json_decode($conf, true);
$conf['dir'] = substr(dirname($plugin), 2);
if($conf['dir'] == 'xn_mobile' || $conf['dir'] == 'qt_tag') continue;
$plcs[] = $conf;
}
$text = '<table class="table"><thead><th>目录</th><th>名称</th><th>描述</th><th>版本</th><th>BBS 版本</th></thead><tbody>';
foreach($plcs as $c) {
$text.= '<tr><td>'.$c['dir'].'</td><td>'.$c['name'].'</td><td>'.$c['brief'].'</td><td>'.$c['version'].'</td><td>'.$c['bbs_version'].'</td></tr>';
}
$text.='</tbody></table>';
file_put_contents('list.txt', $text);

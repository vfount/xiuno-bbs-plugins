<?php
function qt_urlencode($s) {
	$s = base64_encode($s);
	$s = str_replace('-', '_2d', $s);
	$s = str_replace('.', '_2e', $s);
	$s = str_replace('+', '_2b', $s);
	$s = str_replace('=', '_3d', $s);
	$s = urlencode($s);
	$s = str_replace('%', '_', $s);
	return $s;
}

function qt_urldecode($s) {
	$s = str_replace('_', '%', $s);
	$s = urldecode($s);
	$s = base64_decode($s);
	return $s;
}

function qt_humansize($num) {
	if($num > 1073741824) {
		return number_format($num / 1073741824, 2, '.', '').'G';
	} elseif($num > 1048576) {
		return number_format($num / 1048576, 2, '.', '').'M';
	} elseif($num > 1024) {
		return number_format($num / 1024, 2, '.', '').'K';
	} else {
		return $num.'B';
	}
}

function qt_strip_tags($s) {
$s = str_replace("p><p","p>\n<p",$s);
$s = strip_tags($s);
$s = trim($s);
$s = str_replace("&nbsp;","",$s);
$s = str_replace("&ldquo;","“",$s);
$s = str_replace("&rdquo;","”",$s);
$s = str_replace("\r","\n",$s);
$s = str_replace("\n\n","\n",$s);
$s = str_replace("\n\n","\n",$s);
$s = str_replace("\n","\r\n",$s);
return $s;
}
